# toy-async-rust

While you think that you understand async in rust, you will understand that you may be wrong!

This was my case, and this is one reason that I start this toy repository and take some of my old code that I wrote
when I studied the first time async Rust, and rewrite it in a better way.

The goal of this repository if write enough example to complete my onboarding on async rust working group and help
to improve this awesome adventure.

If you want contribute feel free to open a issue!

Cheers!

Vincent.
