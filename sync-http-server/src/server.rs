//! Sync Server implementation to handle toy HTTP request, and return a
//! toy response.
use log::info;
use std::fs;
use std::io::{Read, Write};
use std::net::{TcpListener, TcpStream};

pub struct ToySyncServer {
    host: String,
    port: String,
    root_path: String,
}

impl ToySyncServer {
    pub fn new(host: &str, port: &str, root: &str) -> Self {
        info!("host: {}:{} with res dir: {}", host, port, root);
        return ToySyncServer {
            host: host.to_string(),
            port: port.to_string(),
            root_path: root.to_string(),
        };
    }

    pub fn listen(&self) {
        let listener = TcpListener::bind(format!("{}:{}", self.host, self.port)).unwrap();

        for stream in listener.incoming() {
            let mut stream = stream.unwrap();
            self.handle_request(&mut stream);
        }
    }

    fn handle_request(&self, mut stream: &TcpStream) {
        // Read the first 1024 bytes of data from the stream
        let mut buffer = [0; 1024];
        stream.read(&mut buffer).unwrap();

        let get = b"GET / HTTP/1.1\r\n";

        // Respond with greetings or a 404,
        // depending on the data in the request
        let (status_line, filename) = if buffer.starts_with(get) {
            ("HTTP/1.1 200 OK\r\n\r\n", "index.html")
        } else {
            ("HTTP/1.1 404 NOT FOUND\r\n\r\n", "404.html")
        };
        let contents = fs::read_to_string(format!("{}/{}", self.root_path, filename)).unwrap();

        // Write response back to the stream,
        // and flush the stream to ensure the response is sent back to the client
        let response = format!("{status_line}{contents}");
        stream.write_all(response.as_bytes()).unwrap();
        stream.flush().unwrap();
    }
}
