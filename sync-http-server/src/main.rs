use std::env;
mod server;
use server::ToySyncServer;

fn main() {
    env_logger::init();
    let mut bin_path = env::current_dir().unwrap().as_os_str().to_owned();
    bin_path.push("/res");
    let res = env::var_os("SYNC_RES").unwrap_or(bin_path.to_os_string());
    let server = ToySyncServer::new("127.0.0.1", "9080", res.to_str().unwrap());
    server.listen();
}
