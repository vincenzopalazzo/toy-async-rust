# Sync HTTP Server!

This is a simple implementation for the final project of the final book project, that is a http server implementation.

With this crate the async version of the crate is implemented, by using async-std and futures crates.

## How to Run

The implementation is a little bit different from the book, and to run it you can run the following command

```basb
RUST_LOG=info cargo run --bin sync-http-server
```
