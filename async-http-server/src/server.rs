//! Sync Server implementation to handle toy HTTP request, and return a
//! toy response.
use async_std::io::{ReadExt, WriteExt};
use async_std::net::{TcpListener, TcpStream};
use async_std::task::spawn;
use futures::stream::StreamExt;
use log::info;
use std::fs;

pub struct ToySyncServer {
    host: String,
    port: String,
    root_path: String,
}

impl ToySyncServer {
    pub fn new(host: &str, port: &str, root: &str) -> Self {
        info!("host: {}:{} with res dir: {}", host, port, root);
        return ToySyncServer {
            host: host.to_string(),
            port: port.to_string(),
            root_path: root.to_string(),
        };
    }

    pub async fn listen(&self) {
        let listener = TcpListener::bind(format!("{}:{}", self.host, self.port))
            .await
            .unwrap();

        listener
            .incoming()
            .for_each_concurrent(/* limit */ None, |tcpstream| async move {
                let tcpstream = tcpstream.unwrap();
                spawn(Self::handle_request(tcpstream, self.root_path.to_string()));
            })
            .await;
    }

    async fn handle_request(mut stream: TcpStream, root_path: String) {
        // Read the first 1024 bytes of data from the stream
        let mut buffer = [0; 1024];
        stream.read(&mut buffer).await.unwrap();

        let get = b"GET / HTTP/1.1\r\n";

        // Respond with greetings or a 404,
        // depending on the data in the request
        let (status_line, filename) = if buffer.starts_with(get) {
            ("HTTP/1.1 200 OK\r\n\r\n", "index.html")
        } else {
            ("HTTP/1.1 404 NOT FOUND\r\n\r\n", "404.html")
        };
        let contents = fs::read_to_string(format!("{}/{}", root_path, filename)).unwrap();

        // Write response back to the stream,
        // and flush the stream to ensure the response is sent back to the client
        let response = format!("{status_line}{contents}");
        stream.write(response.as_bytes()).await.unwrap();
        stream.flush().await.unwrap();
    }
}
