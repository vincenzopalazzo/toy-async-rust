use rand::Rng;
use std::time::SystemTime;
use toy_async_runtime::features::Sleep;
use toy_async_runtime::runitime::{block_on, spawn, wait};

fn main() {
    println!("Running mini redis with toy runtime");
    // Vincent implementation!
    block_on(async {
        println!("Start");
        spawn(async { println!("Print before 1") });
        spawn(async { println!("Aleluia") });
        Sleep::new(20000).await;
        println!("End");
    });

    block_on(async {
        const SECOND: u128 = 1000; //ms
        println!("Begin Asynchronous Execution");
        // Create a random number generator so we can generate random numbers
        let mut rng = rand::thread_rng();

        // A small function to generate the time in seconds when we call it.
        let time = || {
            SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs()
        };

        // Spawn 5 different futures on our executor
        for i in 0..5 {
            // Generate the two numbers between 1 and 9. We'll spawn two futures
            // that will sleep for as many seconds as the random number creates
            let random = rng.gen_range(1..10);
            let random2 = rng.gen_range(1..10);

            // We now spawn a future onto the runtime from within our future
            spawn(async move {
                println!("Spawned Fn #{:02}: Start {}", i, time());
                // This future will sleep for a certain amount of time before
                // continuing execution
                Sleep::new(SECOND * random).await;
                // After the future waits for a while, it then spawns another
                // future before printing that it finished. This spawned future
                // then sleeps for a while and then prints out when it's done.
                // Since we're spawning futures inside futures, the order of
                // execution can change.
                spawn(async move {
                    Sleep::new(SECOND * random2).await;
                    println!("Spawned Fn #{:02}: Inner {}", i, time());
                });
                println!("Spawned Fn #{:02}: Ended {}", i, time());
            });
        }
        // To demonstrate that block_on works we block inside this future before
        // we even begin polling the other futures.
        block_on(async {
            // This sleeps longer than any of the spawned functions, but we poll
            // this to completion first even if we await here.
            Sleep::new(11000).await;
            println!("Blocking Function Polled To Completion");
        });
    });

    // We now wait on the runtime to complete each of the tasks that were
    // spawned before we exit the program
    wait();
    println!("Closing client");
}
